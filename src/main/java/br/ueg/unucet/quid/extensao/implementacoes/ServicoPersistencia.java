package br.ueg.unucet.quid.extensao.implementacoes;

import java.util.Collection;

import br.ueg.unucet.quid.extensao.enums.TiposServicoEnum;
import br.ueg.unucet.quid.extensao.implementacoes.SuperServico;
import br.ueg.unucet.quid.extensao.interfaces.IParametro;
import br.ueg.unucet.quid.extensao.interfaces.IServico;
import br.ueg.unucet.quid.extensao.interfaces.ITipoMembroModelo;
//import org.springframework.transaction.annotation.Transactional;

public abstract class ServicoPersistencia extends SuperServico {
	
	public static final String PARAMETRO_VERSAO = "VERSAO";
	public static final String PARAMETRO_REVISAO = "REVISAO";
	public static final String PARAMETRO_VERSAO_ARTEFATO = "VERSAO_ARTEFATO";
	public static final String PARAMETRO_REVISAO_ARTEFATO = "REVISAO_ARTEFATO";
	public static final String PARAMETRO_ID_ARTEFATO_PREENCHIDO = "ID_ARTEFATO_PREENCHIDO";
	public static final String PARAMETRO_LISTA_MEMBROS = "PARAMETRO_LISTA_MEMBROS";
	public static final String PARAMETRO_MODO = "PARAMETRO_MODO";
	public static final String PARAMETRO_NOVA_VERSAO = "PARAMETRO_NOVA_VERSAO";
	public static final String PARAMETRO_VALORES_MEMBROS = "PARAMETRO_VALORES_MEMBROS";
	public static final String MODO_SALVAR = "SALVAR";
	public static final String MODO_LEITURA = "LEITURA";
	
	//private IArtefatoPreenchidoControle<ArtefatoPreenchido, Long> artefatoPreenchidoControle;
	
	public ServicoPersistencia() {
		super();
	}

	@Override
	public ITipoMembroModelo getTipoMembroModelo() {
		return null;
	}
	
	@Override
	public TiposServicoEnum getTipoServico() {
		return TiposServicoEnum.PERSISTENCIA;
	}
	
	@Override
	public Collection<IParametro<?>> getListaParametros() {
		Collection<IParametro<?>> listaParametro =  super.getListaParametros();
		return listaParametro;
	}

	@Override
	public void setTipoMembroModelo(ITipoMembroModelo tipoMembroModelo) {
	}

	@Override
	public String getNomeTipoMembroModelo() {
		return null;
	}

	@Override
	public Integer getVersaoTipoMembroModelo() {
		return null;
	}

	@Override
	public boolean isListaParametrosValidos() {
		return true;
	}

	@Override
	public boolean isListaParametrosCompativeis() {
		return true;
	}

	@Override
	public Collection<String> getNomesParametrosInvalidos() {
		return null;
	}

	@Override
	public IServico getAnterior() {
		return null;
	}

	@Override
	public IServico getProximo() {
		return null;
	}

	@Override
	public void setAnterior(IServico servico) {
	}

	@Override
	public void setProximo(IServico servico) {
	}

	@Override
	public boolean isAnteriorObrigatorio() {
		return false;
	}

	@Override
	public void setAnteriroObrigatorio(boolean obrigatorio) {
	}

	@Override
	public Integer getVersao() {
		return 0;
	}

	@Override
	public Integer getRevisao() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see br.ueg.unucet.quid.servicosquid.SuperServico#efetuaAcao()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected abstract Collection<IParametro> efetuaAcao() throws Exception ;
	
	public Integer getParametroVersao() {
		Object valor = getParametroPorNome(PARAMETRO_VERSAO).getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}
	
	public Integer getParametroRevisao() {
		Object valor = getParametroPorNome(PARAMETRO_REVISAO).getValor();
		if (valor != null) {
			return Integer.valueOf(String.valueOf(valor));
		}
		return null;
	}
	
	public Long getParametroIdArtefatoPreenchido() {
		Object valor = getParametroPorNome(PARAMETRO_ID_ARTEFATO_PREENCHIDO).getValor();
		if (valor != null) {
			return Long.valueOf(String.valueOf(valor));
		}
		return null;
	}
	
	public byte[] getParametroListaMembros() {
		Object valor = getParametroPorNome(PARAMETRO_LISTA_MEMBROS).getValor();
		if (valor != null) {
			return (byte[]) valor;
		}
		return null;
	}
	
	public byte[] getParametroValoresMembros() {
		Object valor = getParametroPorNome(PARAMETRO_VALORES_MEMBROS).getValor();
		if (valor != null) {
			return (byte[]) valor;
		}
		return null;
	}
	
	public String getParametroModo() {
		Object valor = getParametroPorNome(PARAMETRO_MODO).getValor();
		if (valor != null) {
			return String.valueOf(valor);
		}
		return null;
	}
	
	public String getParametroVersaoArtefato() {
		Object valor = getParametroPorNome(PARAMETRO_VERSAO_ARTEFATO).getValor();
		if (valor != null) {
			return String.valueOf(valor);
		}
		return null;
	}
	public String getParametroRevisaoArtefato() {
		Object valor = getParametroPorNome(PARAMETRO_REVISAO_ARTEFATO).getValor();
		if (valor != null) {
			return String.valueOf(valor);
		}
		return null;
	}
	
	public Boolean getParametroNovaVersao() {
		Object valor = getParametroPorNome(PARAMETRO_NOVA_VERSAO).getValor();
		if (valor != null) {
			return (Boolean) valor;
		}
		return null;
	}

}
