package br.ueg.unucet.quid.extensao.utilitarias;

import br.ueg.unucet.quid.extensao.enums.DominioEntradaEnum;
import br.ueg.unucet.quid.extensao.implementacoes.Parametro;

public class FabricaParametros { 

	public static <T> Parametro<T> gerarParametro(Class<T> classParametro,String nome, String rotulo,DominioEntradaEnum dominio,Boolean isObrigatorio)
	{
		Parametro<T> parametro = new Parametro<T>(classParametro);
		parametro.setNome(nome);
		parametro.setRotulo(rotulo);
		parametro.setDominioEntrada(dominio);
		parametro.setObrigatorio(isObrigatorio);
		return parametro;
	}
	
	public static <T> Parametro<T> gerarParametroValor(Class<T> classParametro,String nome, T valor)
	{
		Parametro<T> parametro = new Parametro<T>(classParametro);
		parametro.setNome(nome);
		parametro.setValorClass(valor);
		return parametro;
	}
}
