package br.ueg.unucet.quid.extensao.enums;

/**
 * Enum que identifica os possiveis status de um componente no framework.
 * @author QUID
 *
 */
public enum TiposServicoEnum {
	PERSISTENCIA,ACAO,VALIDACAO,DEPENDENCIA
}
