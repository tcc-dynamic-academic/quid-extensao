package br.ueg.unucet.quid.extensao.enums;

/**
 * Enum que identifica os possiveis momentos de disparo de um serviço no framework.
 * @author QUID
 *
 */
public enum MomentosDisparoServicoEnum {
	AO_ABRIR_ARTEFATO, DURANTE_PREENCHIMENTO_AO_SER_CRIADO, DURANTE_PREENCHIMENTO_AO_PERDER_O_FOCO,
	DURANTE_PREENCHIMENTO_AO_CLICAR, AO_FINALIZAR_PREENCHIMENTO
}